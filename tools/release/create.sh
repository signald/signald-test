#!/bin/bash
set -exuo pipefail

version="$1"

# this script creates a new release.

# update debian changelog to mark $VERSION released
debchange --release --version "${version}" ""

# make a commit with the released changelog
git commit -am "Release signald ${version}"

# tag the commit
git tag --annotate --message "${version}" "${version}"
